USE DW_Control
GO

INSERT INTO etl_package_config VALUES 
('ETL_daily','Website_Activities',1,null,GETDATE(),NULL),
('ETL_daily','Website_Activity_Departments',1,null,GETDATE(),NULL),
('ETL_daily','Website_ActivityRegistrationWindows',1,null,GETDATE(),NULL),
('ETL_daily','Website_Centers',1,null,GETDATE(),NULL),
('ETL_daily','Website_DCProgramFees',1,null,GETDATE(),NULL),
('ETL_daily','Website_DCPrograms',1,null,GETDATE(),NULL),
('ETL_daily','Website_DCProgramSessions',1,null,GETDATE(),NULL),
('ETL_daily','Website_DCSessions',1,null,GETDATE(),NULL),
('ETL_daily','Website_Facilities',1,null,GETDATE(),NULL),
('ETL_daily','Website_Master',1,null,GETDATE(),NULL),
('ETL_daily','Website_RG_Category',1,null,GETDATE(),NULL),
('ETL_daily','Website_RG_Sub_Category',1,null,GETDATE(),NULL),
('ETL_daily','Website_Sites',1,null,GETDATE(),NULL)

INSERT INTO ssis_configuration (ConfigurationFilter,ConfiguredValue,PackagePath,ConfiguredValueType) VALUES
('webmaster','Data Source=ygsactivelive.database.windows.net;User ID=ygsadmin;Password=4YouthDev909;Initial Catalog=ygsactivelivedb;Provider=SQLNCLI11.1;','\Package.Connections[WebSQL].Properties[ConnectionString]','String'),
('webmaster','Data Source=YGSProdDW;Initial Catalog=ActiveLive;Provider=SQLNCLI11.1;Integrated Security=SSPI;','\Package.Connections[ActiveLive].Properties[ConnectionString]','String'),
('webmaster','F:\ETL_WebSite','\Package.Variables[User::etl_folder_path].Properties[Value]','String'),
('Website_Activities','ETL_daily','\Package.Variables[User::load_name].Properties[Value]','String'),
('Website_Activity_Departments','ETL_daily','\Package.Variables[User::load_name].Properties[Value]','String'),
('Website_ActivityRegistrationWindows','ETL_daily','\Package.Variables[User::load_name].Properties[Value]','String'),
('Website_Centers','ETL_daily','\Package.Variables[User::load_name].Properties[Value]','String'),
('Website_DCProgramFees','ETL_daily','\Package.Variables[User::load_name].Properties[Value]','String'),
('Website_DCPrograms','ETL_daily','\Package.Variables[User::load_name].Properties[Value]','String'),
('Website_DCProgramSessions','ETL_daily','\Package.Variables[User::load_name].Properties[Value]','String'),
('Website_DCSessions','ETL_daily','\Package.Variables[User::load_name].Properties[Value]','String'),
('Website_Facilities','ETL_daily','\Package.Variables[User::load_name].Properties[Value]','String'),
('Website_Master','ETL_daily','\Package.Variables[User::load_name].Properties[Value]','String'),
('Website_RG_Category','ETL_daily','\Package.Variables[User::load_name].Properties[Value]','String'),
('Website_RG_Sub_Category','ETL_daily','\Package.Variables[User::load_name].Properties[Value]','String'),
('Website_Sites','ETL_daily','\Package.Variables[User::load_name].Properties[Value]','String'),
('Website_Master',' shill@seattleymca.org; tchamberlain@seattleymca.org;jnilles@seattleymca.org','\Package.Variables[User::SourceEmailRecipients].Properties[Value]','String')